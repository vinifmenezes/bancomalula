package br.com.bancomalula;

public class BancoTeste {

	public static void main(String[] args) {
		
		Conta.saldoDoBanco = 5_000.00;
		
		Conta conta = new Conta(1, "Corrente", 2_750.00, "123!", new Cliente("Vincius", "689652538", "89563214589", "vini.f.menezes@gmail.com", Sexo.MASCULINO));
		
		conta.exibesaldo();
		conta.deposita(50);
		conta.exibesaldo();
		System.out.println("Cofre: " + Conta.saldoDoBanco);
		conta.saca(100);
		conta.exibesaldo();
		System.out.println("Cofre: " + Conta.saldoDoBanco);
		
		// mostrar os dados da conta
		System.out.println("DADOS DA CONTA: ");
		System.out.println("N� " + conta.getNumero());
		System.out.println("Tipo: " + conta.getTipo());
		System.out.println("Senha: " + conta.getSenha());
		System.out.println("Saldo: " + conta.getSaldo());
		System.out.println("Titular: " + conta.getCliente().getNome());
		System.out.println("CPF: " + conta.getCliente().getCpf());
		System.out.println("RG: " + conta.getCliente().getRg());
		System.out.println("E-mail: " + conta.getCliente().getEmail());
		System.out.println("Sexo: " + conta.getCliente().getSexo().nome);

	}

}
